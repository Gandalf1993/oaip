﻿// Лабораторная 12.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <fstream>
#include <ctime>
#include <vector>
#include <string>
#include <math.h>
using namespace std;

void sort(int* num, int size)
{
	for (int i = 0; i < size - 1; i++)
	{
		for (int j = (size - 1); j > i; j--)
		{
			if (num[j - 1] > num[j])
			{
				int tmp = num[j - 1];
				num[j - 1] = num[j];
				num[j] = tmp;
			}
		}
	}
}

void task2()
{
	int matrix[25];

	ofstream text1("task2.txt");
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			text1 << rand() % 100 << " ";
		}
		text1 << endl;
	}
	text1.close();
	ifstream text2("task2.txt");
	for (int i = 0; i < 25; i++)
	{
		text2 >> matrix[i];
	}
	text2.close();
	sort(matrix, 25);
	ofstream text3("task2.txt", ios::app);
	text3 << "\n\n\n";
	for (int i = 1; i < 26; i++)
	{
		text3 << matrix[i - 1] << " ";

		if (i % 5 == 0) text3 << "\n";
	}
	text3.close();
}

void task3()
{
	ofstream text1("task3.txt");
	text1 << "Грядой клубится белою\n"
		"Над озером туман;\n"
		"Тоскою добрый молодец\n"
		"И горем обуян.\n"
		"Не довеку белеется\n"
		"Туманная гряда,\n"
		"Рассеется, развеется,\n"
		"А горе никогда!\n";
	text1.close();
	ifstream text2("task3.txt");
	int lines = 0, symbols = 0;
	string str;
	while (getline(text2, str))
	{
		lines++;
		symbols += str.size();
	}
	text2.close();
	cout << "Lines: " << lines << "\nSymbols: " << symbols;
}

void task4()
{
	vector<string> vector;
	string str1;
	int paragraph;
	cout << "\n\nНомер абзаца для удаления: ";
	cin >> paragraph;
	ofstream text("task4.txt");
	text << "      Грядой клубится белою\n"
		"Над озером туман;\n"
		"     Тоскою добрый молодец\n"
		"И горем обуян.\n"
		"Не довеку белеется\n"
		"     Туманная гряда,\n"
		"Рассеется, развеется,\n"
		"     А горе никогда!\n";

	text.close();
	int paragraph_counting = 0;
	ifstream text1("task4.txt");
	while (getline(text1, str1))
	{
		if (str1[0] == ' ' && str1[1] == ' ' && str1[2] == ' ' && str1[3] == ' ' && str1[4] == ' ')
			paragraph_counting++;

		if (paragraph_counting != paragraph)
			vector.push_back(str1);
	}
	ofstream text2("task4.txt", ios::app);
	text2 << "\n\n\n";

	for (int i = 0; i < vector.size(); i++)
	{
		text2 << vector[i] << '\n';
	}
	text2.close();
}

void task5()
{
	ofstream text("task5.txt");
	text << "Грядой клубится белою\n"
		"Над озером туман;\n"
		"Тоскою добрый молодец\n"
		"И горем обуян.\n"
		"Не довеку белеется\n"
		"Туманная гряда,\n"
		"Рассеется, развеется,\n"
		"А горе никогда!\n";
	text.close();
	ifstream text1("task5.txt");
	ofstream text2("task5.1.txt");
	char ch;
	while (text1 >> ch)
		if (ispunct(static_cast<unsigned char>(ch)))
			text2 << ch;
	text1.close();
	text2.close();
}

int main()
{
	setlocale(LC_ALL, "ru");
	void (*tasks[4])() = { task2, task3, task4, task5 };
	int choice = 0;
	while (choice != 5)
	{
		cout << "1. Сортировка обменом\n"
			<< "2. Подсчет символов и строк\n"
			<< "3. Удаление абзаца\n"
			<< "4. Вывод знаков препинания\n"
			<< "5. Выход\n"
			<< "Введите номер функции(1-5): ";

		cin >> choice;

		switch (choice)
		{
		case 1:
		{
			tasks[0]();
			cout << "\nОтсортировано\n\n";
			break;
		}
		case 2:
		{
			cout << "\n";
			tasks[1]();
			cout << "\n\n";
			break;
		}
		case 3:
		{
			tasks[2]();
			cout << "\nАбзац удален\n\n";
			break;
		}
		case 4:
		{
			tasks[3]();
			cout << "\nЗнаки препинания выведены\n\n";
			break;
		}
		case 5:
		{
			break;
		}
		}
	}
	return 0;
}


// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
