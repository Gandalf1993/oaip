﻿// Лабораторная 9.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <locale.h>
#include <conio.h>
#include <Windows.h>

using namespace std;

struct Human {
    char Fam[100]; // Фамилия 
    char Im[100]; // Имя
    char Ot[100]; // Отчество
    char DA[100]; // Домашний адрес
    char PN[100]; // Номер телефона
    unsigned Y; // Возраст
    int N; // Номер человека
    bool sint;
};

void output_h(Human h) {
    cout << "Номер человека " << h.N << endl << endl;
    cout << "Фамилия " << h.Fam << endl;
    cout << "Имя " << h.Im << endl;
    cout << "Отчество " << h.Ot << endl;
    cout << "Домашний адрес " << h.DA << endl;
    cout << "Номер телефона " << h.PN << endl;
    cout << "Возраст " << h.Y << endl << endl;
}

int main()
{
    Human* h = new Human[1];
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int HN; // Кол-во человек
    double kh = false;
    while (1) { // меню
        enum menu { input = 1, output, sort, search, change, sdelete, sexit };
        int choice = 0;
        while (choice != 7) {
            cout << "\n\n";
            cout << "1.Ввод массива людей\n";
            cout << "2.Вывод массива людей\n";
            cout << "3.Сортировка массива людей\n";
            cout << "4.Поиск в массиве людей по заданному параметру\n";
            cout << "5.Изменение заданного человека \n";
            cout << "6.Удаление человека из массива\n";
            cout << "7.Выход\n";
            cout << "Выберите пункт меню: ";
            cin >> choice;
            switch (choice) {
            case input: {
                kh = true;
                cout << "Кол-во человек = ";
                cin >> HN;
                h = new Human[HN];
                for (int i = 0; i < HN; i++) {
                    h[i].N = i + 1;
                    cout << "Номер человека: " << h[i].N << endl << endl;
                    cout << "Фамилия: "; cin >> h[i].Fam; cout << endl;
                    cout << "Имя: "; cin >> h[i].Im; cout << endl;
                    cout << "Отчество: "; cin >> h[i].Ot; cout << endl;
                    cout << "Домашний адрес: "; cin >> h[i].DA; cout << endl;
                    cout << "Номер телефона: "; cin >> h[i].PN; cout << endl;
                    cout << "Возраст: "; cin >> h[i].Y; cout << endl;
                }
                break;
            }
            case output: {
                cout << "Вывод массива людей\n";
                for (int i = 0; i < HN; i++) {
                    output_h(h[i]);
                }
                break;
            }
            case sort: {
                int sort;
                cout << "1. По фамилии \n 2. По возрасту \n 3. По домашнему адресу \n Ввод: ";
                cin >> sort;
                Human h_copy;
                for (int i = 0; i < (HN - 1); i++) {
                    if (sort == 1) {
                        if (h[i].Fam > h[i + 1].Fam) {
                            int h_i_N = h[i].N;
                            h[i].N = h[i + 1].N;
                            h[i + 1].N = h_i_N;
                            h[i] = h[i + 1];
                            h[i + 1] = h_copy;
                        }
                    }
                    else if (sort == 2) {
                        if (h[i].Y > h[i + 1].Y) {
                            int h_i_N = h[i].N;
                            h[i].N = h[i + 1].N;
                            h[i + 1].N = h_i_N;
                            h_copy = h[i];
                            h[i] = h[i + 1];
                            h[i + 1] = h_copy;
                        }
                    }
                    else if (sort == 3) {
                        if (h[i].DA > h[i + 1].DA) {
                            int h_i_N = h[i].N;
                            h[i].N = h[i + 1].N;
                            h[i + 1].N = h_i_N;
                            h_copy = h[i];
                            h[i] = h[i + 1];
                            h[i + 1] = h_copy;
                        }
                    }
                }
                break;
            }
            case search: {
                int search;
                cout << "1. По фамилии\n 2. По имени\n 3. По отчеству\n 4. По домашнему адресу\n 5. По номеру телефона\n 6. По возрасту\n";
                cin >> search;
                if (search == 1) {
                    char Fam[100] = ""; cout << "Ввод:"; cin >> Fam;
                    for (int i = 0; i < HN; i++)
                        if (!strcmp(Fam, h[i].Fam))
                            output_h(h[i]);
                }
                if (search == 2) {
                    char Im[100] = ""; cout << "Ввод:"; cin >> Im;
                    for (int i = 0; i < HN; i++)
                        if (!strcmp(Im, h[i].Im))
                            output_h(h[i]);
                }
                if (search == 3) {
                    char Ot[100] = ""; cout << "Ввод:"; cin >> Ot;
                    for (int i = 0; i < HN; i++)
                        if (!strcmp(Ot, h[i].Ot))
                            output_h(h[i]);
                }
                if (search == 4) {
                    char DA[100]; cout << "Ввод:"; cin >> DA;
                    for (int i = 0; i < HN; i++)
                        if (!strcmp(DA, h[i].DA))
                            output_h(h[i]);
                }
                if (search == 5) {
                    char PN[100] = ""; cout << "Ввод:"; cin >> PN;
                    for (int i = 0; i < HN; i++)
                        if (!strcmp(PN, h[i].PN))
                            output_h(h[i]);
                }
                if (search == 6) {
                    int Y; cout << "Ввод:"; cin >> Y;
                    for (int i = 0; i < HN; i++)
                        if (Y == h[i].Y)
                            output_h(h[i]);
                }
                break;
            }
            case change: {
                unsigned short N;
                cout << "Номер человека: "; cin >> N;
                unsigned short i = N - 1;
                cout << endl << endl;
                for (int i = 0; i < HN; i++) {
                    cout << "Фамилия: " << h[i].Fam << "\n";
                    cout << "Требуются ли изменения? Если нет - нажмите 1, если да - нажмите 2: ";
                    int L; cin >> L;
                    if (L == 2) {
                        cout << "Фамилия: "; cin >> h[i].Fam; cout << endl;
                    }
                    else {
                        continue;
                    }
                }
                for (int i = 0; i < HN; i++) {
                    cout << "Имя: " << h[i].Im << "\n";
                    cout << "Требуются ли изменения? Если нет - нажмите 1, если да - нажмите 2: ";
                    int L; cin >> L;
                    if (L == 2) {
                        cout << "Имя: "; cin >> h[i].Im; cout << endl;
                    }
                    else {
                        continue;
                    }
                }
                for (int i = 0; i < HN; i++) {
                    cout << "Отчество: " << h[i].Ot << "\n";
                    cout << "Требуются ли изменения? Если нет - нажмите 1, если да - нажмите 2: ";
                    int L; cin >> L;
                    if (L == 2) {
                        cout << "Отчество: "; cin >> h[i].Ot; cout << endl;
                    }
                    else {
                        continue;
                    }
                }
                for (int i = 0; i < HN; i++) {
                    cout << "Домашний адрес: " << h[i].DA << "\n";
                    cout << "Требуются ли изменения? Если нет - нажмите 1, если да - нажмите 2: ";
                    int L; cin >> L;
                    if (L == 2) {
                        cout << "Домашний адрес: "; cin >> h[i].DA; cout << endl;
                    }
                    else {
                        continue;
                    }
                }
                for (int i = 0; i < HN; i++) {
                    cout << "Номер телефона: " << h[i].PN << "\n";
                    cout << "Требуются ли изменения? Если нет - нажмите 1, если да - нажмите 2: ";
                    int L; cin >> L;
                    if (L == 2) {
                        cout << "Номер телефона: "; cin >> h[i].PN; cout << endl;
                    }
                    else {
                        continue;
                    }
                }
                for (int i = 0; i < HN; i++) {
                    cout << "Возраст: " << h[i].Y << "\n";
                    cout << "Требуются ли изменения? Если нет - нажмите 1, если да - нажмите 2: ";
                    int L; cin >> L;
                    if (L == 2) {
                        cout << "Возраст: "; cin >> h[i].Y; cout << endl;
                    }
                    else {
                        continue;
                    }
                }
                break;
            }
            case sdelete: {
                unsigned short N;
                cout << "Номер человека: "; cin >> N;
                if (HN == 1) {
                    delete[] h;
                    h = new Human[1];
                    kh = false;
                    continue;
                }
                HN = HN - 1;
                for (unsigned short i = N - 1; i < HN; i++) {
                    int h_i_N = h[i].N;
                    h[i].N = h[i + 1].N;
                    h[i + 1].N = h_i_N;
                    h[i] = h[i + 1];
                }
                Human* h_copy = new Human[HN];
                for (int i = 0; i < HN; i++) {
                    h_copy[i] = h[i];
                }
                delete[] h;
                h = new Human[HN];
                for (int i = 0; i < HN; i++) {
                    h[i] = h_copy[i];
                }
                break;
            }
            case sexit: {
                break;
            }
            }
        }
    }
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
