﻿// Лаба10.1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>							// Задание 1		
#include <ctime>
using namespace std;

void Invert(int A[], const int N)
{
	cout << "Изначальный массив: \t";
	for (int i = 0; i < N; i++) {
		cout << A[i] << "  ";
	}
	cout << "\nИнвертированный массив: ";
	for (int i = 0; i < N / 2; i++) {
		swap(A[i], A[N - 1 - i]);
	}
	for (int i = 0; i < N; i++) {
		cout << A[i] << "  ";
	}
}

int main()
{
	setlocale(LC_ALL, "ru");
	srand(time(0));
	const int NA = 5;
	const int NB = 10;
	const int NC = 15;
	int A[NA];
	int B[NB];
	int C[NC];
	for (int i = 0; i < NA; i++)
	{
		A[i] = rand() % 100;
	}
	for (int i = 0; i < NB; i++)
	{
		B[i] = rand() % 100;
	}
	for (int i = 0; i < NC; i++)
	{
		C[i] = rand() % 100;
	}
	Invert(A, NA);
	Invert(B, NB);
	cout << "\n";
	Invert(C, NC);
	cout << "\n\n\n";
	return 0;
}


// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
